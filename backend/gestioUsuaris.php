<?php
    session_start();
    if(!isset($_SESSION['usu_nom'])){
        session_destroy();
        header('location: login.php?error=Has de logarte per entrar a l\'espai personal!');
    }else{
        include("includes/head.html");
        include("../database/database.php");
    ?>
    <link rel="stylesheet" href="../style/style_table.css">
    <div class="table-box">
    <H2>Benvingut <?php echo $_SESSION['usu_nom']; ?></H2>
    <table class="table-fill">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Nivell</th>
            <th>Modificar</th>
            <th>Borrar</th>

        </tr>
        </thead>
        <tbody>

    <?php
     $resul = mysqli_query($conn, "SELECT * FROM usuari order by usu_id");
        
     while($res = mysqli_fetch_array($resul)){
        echo "<tr>
        <td>$res[usu_id]</td>
        <td>$res[usu_nom]</td>
        <td>$res[usu_nivell]</td>";
        ?>  

        <td>
            <form action="modificarUsuari.php" method='POST'>
                <input type="hidden" value=<?php echo $res['usu_id'];?> name="id">
                <input type="hidden" value=<?php echo $res['usu_nom'];?> name="nom">
                <input type="hidden" value=<?php echo $res['usu_password'];?> name="pass">
                <input type="hidden" value=<?php echo $res['usu_nivell'];?> name="nivell">
                <!-- <input type="submit" value="Modificar"> -->

                <button type="submit" class="btn-modify">
                <i class="fa-sharp fa-solid fa-user-pen"></i>
                </button>
            </form>
        </td>
        <td>
            <form action="eliminarUsuari.proc.php" method="post" onsubmit="return confirm('Segur que vols eliminar aquest usuari?');">
                <input type="hidden" value=<?php echo $res['usu_id'];?> name="id">
                <!-- <input type="submit" value="Borrar"> -->
                <button onclick="myFunction()" type="submit" class="btn-trash">
                <i class="fa-sharp fa-solid fa-trash"></i>
                </button>

            </form>
        </td>
     </tr>
        <?php

     }

    }
    ?>

    </tbody>
    <div class="buttons">        
        <form class="left-button" action="crearUsuari.php" method="post">
            <!-- <input type="submit" value="Crear Usuari"> -->
            <button type="submit" class="btn-modify">
                <i class="fa-solid fa-user-plus"></i>
            </button>
        </form>        
        
        <form class="right-button" action="logout.proc.php" method="post">
            <!-- <input type="submit" value="Tancar sessió"> -->
            <button type="submit" class="btn-trash">
                <i class="fa-solid fa-arrow-right-from-bracket"></i>
            </button>
        </form>  
    </div>

</div>
