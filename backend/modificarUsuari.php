<?php
session_start();
if(!isset($_SESSION['usu_nom'])){
	session_destroy();
	header('location: login.php?error=Has de logarte per entrar a l\'espai personal!');
}else{


    include("includes/head.html");
	if(isset($_POST['id'])){
    $id = $_POST['id'];
    $nom = $_POST['nom'];
    $nivell = $_POST['nivell'];
	$pass = $_POST['pass']

?>
<link rel="stylesheet" href="../style/style_login.css">

<div class="login-box">
<h2>Modificar Usuari</h2>
	<form action="modificarUsuari.proc.php" method="POST">
		<input type="hidden" id="usu_id" name="usu_id" value="<?php echo $id ?>">
		<input type="hidden" id="usu_pas" name="usu_pas" value="<?php echo $pass ?>">
		<div class="user-box">
		<input type="text" id="usu_nom" name="usu_nom" value="<?php echo $nom ?>"required><br><br>
		<label for="usu_nom">Nom d'usuari:</label>
	</div>
	<div class="user-box">

		<input type="text" id="new_pass" name="new_pass" value= "<?php echo $pass ?>"><br><br>
		<label for="new_pass">Contrasenya:</label>
	</div>
		<div class="user-box">

		<input type="text" id="new_pass_confirm" name="new_pass_confirm" value= "<?php echo $pass ?>"><br><br>
		<label for="new_pass_confirm">Repeteix Contrasenya:</label>
	</div>

	<div class="user-box">

		
		<select id="usu_nivell" name="usu_nivell" required>
		<?php
		$values = array("user", "admin");
		foreach ($values as $val) {
			if ($val == $nivell) {
			  echo "<option value=\"$val\" selected>$val</option>";
			} else {
			  echo "<option value=\"$val\">$val</option>";
			}
		  }

		?>
	
		</select>
		<label for="usu_nivell">Nivell d'usuari:</label>
	</div>

		<input type="submit" value="Entrar">
	</form>

<?php
    include("includes/foot.html");
 }
}

?>
