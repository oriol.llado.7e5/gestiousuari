<?php

session_start();
    if(!isset($_SESSION['usu_nom'])){
        session_destroy();
        header('location: login.php?error=Has de logarte per entrar a l\'espai personal!');
    }else{
        include("includes/head.html");
        include("../database/database.php");
?>
<link rel="stylesheet" href="../style/style_login.css">
<div class="login-box">
<h2>Crear Usuari</h3>
	<form action="crearUsuari.proc.php" method="POST">
	<div class="user-box">
		<input type="text" id="usu_nom" name="usu_nom" required><br><br>
		<label for="usu_nom">Nom d'usuari:</label>
	</div>
	<div class="user-box">
		<input type="password" id="new_pass" name="new_pass" required><br><br>	
		<label for="new_pass">Contrasenya:</label>
	</div>
	<div class="user-box">
		<input type="password" id="new_pass_confirm" name="new_pass_confirm" required><br><br>
		<label for="new_pass_confirm">Repateix Contrasenya:</label>

	</div>
	<div class="user-box">

		<select id="usu_nivell" name="usu_nivell" required>
			<option selected disabled>--seleciona opcció--</option>
			<option value="user">Usuari</option>
			<option value="admin">Administrador</option>
		</select>

		<label for="usu_nivell">Nivell d'usuari:</label>

	</div>
		<?php
		if(isset($_GET['error'])){
			echo $_GET['error'];
		}
		?>


		<input type="submit" value="Entrar">
	</form>

<?php
    include("includes/foot.html");
	}
?>
